##
# styleguide
#
# @file
# @version 0.1

.PHONY: all
all: dist/style.css dist/index.html

dist:
	mkdir dist

dist/style.css: dist src/style.scss
	sassc --load-path="vendor/bulma/sass" --load-path="vendor/ibm-plex/scss" --load-path="vendor/bulma-prefers-dark" \
		src/style.scss dist/style.css

dist/index.html: dist src/index.html
	cp src/index.html dist/index.html

.PHONY: clean
clean:
	rm -rf dist/*

WATCH_TARGET ?= "all"

.PHONY: watch
watch:
	while true; do \
		make $(WATCH_TARGET); \
		inotifywait -qre close_write src; \
	done

# end
